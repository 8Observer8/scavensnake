﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour
{
    public int points = 10;
    public float rotationSpeed = 60f;

    void Update()
    {
        transform.Rotate(Vector3.up, rotationSpeed * Time.deltaTime);
    }

    public void Eat()
    {
        GameManager.points += points;

        GameObject player = GameObject.Find("Snake") as GameObject;
        player.GetComponent<Player>().CreateTail();

        Destroy(gameObject);
    }
}
