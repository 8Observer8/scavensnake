﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadLeveByTimer : MonoBehaviour
{
    public float delay = 3f;
    public string levelName;

    public IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(levelName);
    }
}
