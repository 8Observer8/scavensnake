﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class GameManager : MonoBehaviour
{
    public static int points;
    public Text txtPointsUI;

    private int _lastPoints = -1;

    void Awake()
    {
        points = 0;
    }

    void Update()
    {
        // Refresh only if changed
        if (_lastPoints == points) return;

        _lastPoints = points;

        txtPointsUI.text = "Score: " + points.ToString("0000");

        // TODO: temporarily
        if (points == 80)
        {
            StartCoroutine("ShowWinScene");
        }
    }

    IEnumerator ShowWinScene()
    {
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Win");
    }
}
