﻿using UnityEngine;
using System.Collections;

public class Tail : MonoBehaviour
{
    public Transform target;
    public float targetDistance;

    // Update is called once per frame
    void Update()
    {
        Vector3 direction = target.position - transform.position;

        float distance = direction.magnitude;

        if (distance > targetDistance)
        {
            transform.position += direction.normalized * (distance - targetDistance);
            transform.LookAt(target);
        }
    }
}
