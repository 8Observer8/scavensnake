﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class Player : MonoBehaviour
{
    public float speed = 3f;

    public float rotationSpeed = 90f;

    private CharacterController _charController;

    Transform current;

    // Use this for initialization
    void Start()
    {
        _charController = GetComponent<CharacterController>();

        current = transform;
    }

    private bool _testing = false;

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");

        transform.Rotate(0, rotationSpeed * Time.deltaTime * horizontal, 0);

        _testing = true;

        _charController.Move(transform.forward * speed * Time.deltaTime);
    }

    public void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Debug.Log(hit.collider.name);
        if (_testing)
        {
            Food food = hit.collider.GetComponent<Food>();
            if (food != null)
            {
                food.Eat();
                SoundSnakeBarell();
            }
            else
            {
                SceneManager.LoadScene("GameOver");
            }
            _testing = false;
        }
    }

    public void CreateTail()
    {
        Vector3 pos = current.transform.position - current.transform.forward * 1;
        GameObject tail = Instantiate(Resources.Load("Prefabs/TailPrefab"), pos, transform.localRotation) as GameObject;
        tail.GetComponent<Tail>().target = current.transform;
        tail.GetComponent<Tail>().targetDistance = 1f;
        //Destroy(tail.GetComponent<Collider>());
        current = tail.transform;
    }

    private void SoundSnakeBarell()
    {
        AudioSource sound = Instantiate(Resources.Load("Prefabs/SnakeBarellAudioSource", typeof(AudioSource))) as AudioSource;
        sound.Play();
    }
}
